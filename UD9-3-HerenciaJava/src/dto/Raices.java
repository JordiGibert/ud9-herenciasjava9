package dto;
import java.util.ArrayList;

public class Raices {
	
	//Crear atributos y constructor (sencillo)
	private double a;
	private double b;
	private double c;
	public Raices(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	//Obtener discriminante (lo que se le hara una raiz cuadrada
	private double getDiscriminante() {
		double discriminante =(b*b)-(4*a*c);
		return discriminante;
	}
	
	//Comprovar si el discriminante es mayor a 0 para saber el numero de resultados que tendra
	private boolean tieneRaices() {
		boolean raices=false;
		if(getDiscriminante()>0){
			raices=true;
		}
		return raices;
	}
	
	//Comprvar que no sea 0, si es negativo no tendra resultados
	private boolean tieneRaiz() {
		boolean raices=false;
		if(getDiscriminante()==0){
			raices=true;
		}
		return raices;
	}
	
	//Si hay dos raices, calculular el resultado de las dos mas el resto de la ecuacion
	private ArrayList<Double> obtenerRaices() {
		
		ArrayList<Double> raices=new ArrayList<Double>();
		double raiz=Math.sqrt(getDiscriminante());
		raices.add(((-b)+raiz)/(2*a));
		raices.add(((-b)-raiz)/(2*a));
		return raices;
	}
	
	//Si hay un resultado, caluylar el resto de esa equacion
	private double obtenerRaiz() {
		double raiz=(-b)/(2*a);
		return raiz;
	}
	
	//Metodo que llama a otros metodos y muestra por consola si la ecuacion tiene resultado, y si es asi el numero de resultados y sus resultados
	
	public String solucion() {
		String solucion = "";
		if(tieneRaices()) {
			ArrayList<Double> resultados = obtenerRaices();
			solucion="La ecuacion tiene dos resultados: "+resultados.get(0)+" y "+resultados.get(1);
		} else if(tieneRaiz()) {
			solucion= "El resultado de la equacion es: "+obtenerRaiz();
		} else {
			solucion="La equiacion no tiene solucion";
		}
		return solucion;
	}
}
