import dto.Raices;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Crear 3 objetos raizes con los tres tipos de resultados
		Raices ecuacion1=new Raices(5550, 5, 454);
		Raices ecuacion2=new Raices(1, -5, 6);
		Raices ecuacion3=new Raices(1, 4, 4);
		//mostrar soluciones
		System.out.println(ecuacion1.solucion());
		System.out.println(ecuacion2.solucion());
		System.out.println(ecuacion3.solucion());
	}

}
